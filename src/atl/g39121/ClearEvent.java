package atl.g39121;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * Created by audric on 02.03.16.
 */
public class ClearEvent implements EventHandler<ActionEvent> {
    private TextField textField;
    private TextField textField1;
    private TextField textField2;

    public ClearEvent(TextField textField, TextField textField1, TextField textField2) {
        this.textField = textField;
        this.textField1 = textField1;
        this.textField2 = textField2;
    }

    @Override
    public void handle(ActionEvent event) {
        textField.setText("");
        textField1.setText("");
        textField2.setText("");
    }
}
